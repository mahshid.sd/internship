users = [
    {
        avatar: "01.jpg",
        name: "- John Doe",
        qoute: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."
    },
    {
        avatar: "02.jpg",
        name: "- Johnathan Doe",
        qoute: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."
    },
    {
        avatar: "03.jpg",
        name: "- John Doe",
        qoute: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."
    },
    {
        avatar: "04.jpg",
        name: "- Johnathan Doe",
        qoute: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."
    },
    {
        avatar: "05.jpg",
        name: "- John Doe",
        qoute: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."
    },
    {
        avatar: "06.jpg",
        name: "- Johnathan Doe",
        qoute: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."
    }
];

for(var i=0; i<users.length; i++) {
    getavatar(users[i], i);
}

function getavatar(user, i) {
    var userDiv = document.createElement("div");
    userDiv.id = "user__"+ i;
    document.getElementById("users").appendChild(userDiv);
    document.getElementById("user__"+ i).style.width = "300px";
    document.getElementById("user__"+ i).style.display = "flex";
    document.getElementById("user__"+ i).style.margin = "2.4rem";

    var avatar = document.createElement("img");
    avatar.setAttribute("src",user.avatar);
    document.getElementById("user__"+ i).appendChild(avatar);
    avatar.style.borderRadius = "50%";
    avatar.style.height = "70px";

    var userText = document.createElement("div");
    userText.id = "text__"+ i;
    document.getElementById("user__"+ i).appendChild(userText);
    userText.style.marginLeft = "1rem";
    userText.style.fontSize = "16px";
    userText.style.display = "flex";
    userText.style.flexDirection = "column";

    var qoute = document.createElement("q");
    qoute.innerText = user.qoute;
    document.getElementById("text__"+ i).appendChild(qoute);
    qoute.style.textAlign = "left";
    qoute.style.marginBottom = "0.5rem";
    qoute.style.paddingTop ="0rem";


    var name = document.createElement("b");
    name.innerText = user.name;
    document.getElementById("text__"+ i).appendChild(name);
    name.style.textAlign = "left";
    name.style.padding = "0rem"
}

function opnemenu() {
    var x = document.getElementById("myTopnav");
    if (x.className === "nav-bar") {
      x.className += " responsive";
    } else {
      x.className = "nav-bar";
    }
}
  